// (c) Copyright 2010-2012 MCQN Ltd.
// Released under Apache License, version 2.0
//
// Simple example to show how to use the HttpClient library
// Get's the web page given at http://<kHostname><kPath> and
// outputs the content to the serial port

#include <SPI.h>
#include <HttpClient.h>
#include <Ethernet.h>
#include <EthernetClient.h>

#include <Metro.h>

#include <WebServer.h>

#include <LiquidCrystal.h>

#define DEBUG 1;
#define LOGGING 1;

#define PREFIX ""

// Name of the server we want to connect to
const char serverHostname[] = "diploma.loc";
// Path to download (this is the bit after the hostname in the URL
// that you want to download
const char serverPath[] = "/api/";

/*
* Server IP address
*/
const IPAddress serverIp(192,168,180,254);

/**
 * Arduino IP address for NON DHCP networks
 */
const IPAddress ip(192, 168, 180, 229);
/**
 * Current arduino Mac Address
 */
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

// Number of milliseconds to wait without receiving any data before we give up
//Currently 30 seconds
const int kNetworkTimeout = 30*1000;
// Number of milliseconds to wait if no data is available before trying again
const int kNetworkDelay = 1000;

/**
 * Transformator ID into Database
 */ 
const int trans  = 2;

/**
 * Transformator indicator PIN
 */
const int transLed  = 2;

/**
 * PIN for switch off transformator
 */
const int transPowerPin  = 3;

/**
 * Current status of transformator
 */
boolean transStatus = true;

/**
 * Metro interval for checking button state
 */
Metro transStateMetro = Metro(500); 

/**
 * Metro interval for sending data to server
 */
Metro transStatsMetro = Metro(5000); 
/**
 * Ethernet server for Arduino commands on port 80
 */
WebServer server(PREFIX, 80);


// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(29, 28, 25, 24, 23, 22);
//LED for display
const int LedPin  = 47;

/**
 * Voltmeter
 */
/**
 * Analog input for voltmeter
 */
const int voltmeterInput = 8;

Metro voltmeterMetro = Metro(700);

float vout = 0.0;
float vin = 0.0;
float oldVin  = 0.0;
float oldIin = 0.0;
float R1 = 51000.0;    // !! resistance of R1 !!
float R2 = 10000.0;     // !! resistance of R2 !!

// variable to store the voltmeter value 
int value = 0;  

/**
 * Handle request from site
 */
void servOffCmd(WebServer &server, WebServer::ConnectionType type, char *, bool) {
  turnTransformator(false);
  server.httpSuccess();
  #ifdef DEBUG
  Serial.println("OFF");
  #endif
}

void servOnCmd(WebServer &server, WebServer::ConnectionType type, char *, bool) {
  turnTransformator(true);
  server.httpSuccess();
  #ifdef DEBUG
  Serial.println("ON");
  #endif
}

/**
 * default command for server
 */
void serverCmd(WebServer &server, WebServer::ConnectionType type, char *, bool) {
  /* for a GET or HEAD, send the standard "it's all OK headers" */
  server.httpSuccess();
  server.print(transStatus);
  return;
}

void setup()
{
  #ifdef DEBUG
  // initialize serial communications at 9600 bps:
  Serial.begin(9600); 
  Serial.println("Start configure");
  #endif
  /**
  * Get IP address from DHCP
  */
  /** /
  while (Ethernet.begin(mac) != 1)
  {
    #ifdef DEBUG
    Serial.println("Error getting IP address via DHCP, trying again...");
    #endif
    delay(15000);
  } 
 /**/ 
  
  /**
   * Arduino ethernet client for NON DHCP network
   */
   /**/
   Ethernet.begin(mac, ip);
   /**/
   
   /**
    * Setup server
    */
   server.setDefaultCommand(&serverCmd);
   server.addCommand("off", &servOffCmd);
   server.addCommand("on", &servOnCmd);

   /* start the server to wait for connections */
   server.begin();
   
   /**
    * Setup light pins
    */
    pinMode(transLed, OUTPUT);
    turnLight();
    
    /**
     * Setup pin for turning off trans
     */
    pinMode(transPowerPin, INPUT);
    //digitalWrite(transPowerPin, HIGH);
    
    // set up the LCD's number of columns and rows: 
    lcd.begin(16, 2);
    pinMode(LedPin, OUTPUT);
    
    /**
     * Voltmeter setup
     */
    pinMode(voltmeterInput, INPUT);
}

/**
* Main programm cicle
*/ 
void loop()
{
  //lcd.scrollDisplayLeft();
  //check button state
  if (transStateMetro.check() == 1) {
    checkTransPower();
    turnLight();
  }
  //send statistic to server
  if (transStatsMetro.check() == 1) {
    if (transStatus) {
      sendStat(trans, random(40, 60), 1.9, oldVin, oldIin);
    } else {
      #ifdef DEBUG
      Serial.println("Transformator is Turned OFF !");
      #endif
    }
  }
  
  /**
   * Proccess connections for server
   */
  server.processConnection();
  
  /**
   * Voltmeter reading
   */
  if (voltmeterMetro.check() == 1) {
    readVoltmeter();
  }
  //while(1);
}

void readVoltmeter() {
  // read the value on analog input
  value = analogRead(voltmeterInput);

  vout = (value * 5.0) / 1024.0;
  vin = vout / (R2/(R1+R2));  
  boolean shouldChange = (vin > oldVin || vin < oldVin);
  
  if (transStatus && shouldChange) {
    oldVin = vin;
    // print result to lcd display
    lcd.setCursor(2, 1);
    lcd.print(vin);
    lcd.print("V I=");
    float ii = vin / R2 * 1000;
    oldIin = ii;
    lcd.print(ii);
    lcd.print("mA");
  }
}

/**
 * Checks current button state
 */
void checkTransPower() {
  int buttonState = digitalRead(transPowerPin);
  if (buttonState == HIGH) {     
    turnTransformator(!transStatus);
    #ifdef DEBUG
    Serial.println("Turn trans OFF/ON");
    Serial.println(transStatus);
    #endif
  } 
}

/**
 * Turn transformator on/off
 */
void turnTransformator(boolean on) {
  transStatus = on;
  if (on) {
    //sendPower(1);
  } else {
    //sendPower(0);
  }
}

/**
 * Check current trans state and turn on/off LED
 */
void turnLight() {
  if(transStatus) {
    digitalWrite(transLed, HIGH);
    digitalWrite(LedPin, HIGH);
    lcd.clear();
    lcd.setCursor(0, 0); //set to top-left
    lcd.print("Turned ON.");
    lcd.setCursor(0, 1); // set to bottom-left
    lcd.print("V=");
    lcd.setCursor(2, 1);
    lcd.print(oldVin);
    lcd.print("V I=");
    lcd.print(oldIin);
    lcd.print("mA");
  } else {
    digitalWrite(transLed, LOW);
    digitalWrite(LedPin, LOW);
    lcd.clear();
    lcd.setCursor(0, 0); //set to top-left
    lcd.print("Turned OFF.");
  }
}

/**
 * Convert double to char[]
 */
char *doubleToChar(const double var) {
  char diffs[20];
  dtostrf(var, 4, 2, diffs);
  return diffs;
}

/**
 * Send transformator current state
 */
void sendPower(int state) {
  char path[40];
  sprintf (path, "/app_dev.php/api/power/%d/%d/", trans, state);
  sendToServer(path);
}

/**
 * Send statistics to server
 */
void sendStat(const int transId, const int temp, const double diff, const double u, const double i) {
  char path[60];
  char diffs[10];
  dtostrf(diff, 4, 2, diffs);
  char us[10];
  dtostrf(u, 4, 2, us);
  char is[10];
  dtostrf(i, 4, 2, is);
  sprintf (path, "/app_dev.php/api/stat/%d/%d/%s/%s/%s", transId, temp, diffs, us, is);
  sendToServer(path);
}

void sendToServer(char * path) {
  int err = 0;
  
  EthernetClient c;
  HttpClient http(c);
  #ifdef DEBUG
  Serial.println(path);
  #endif
  err = http.get(serverIp, serverHostname, path);
  if (err == 0)
  {
    #ifdef DEBUG
    Serial.println("startedRequest ok");
    #endif
    err = http.responseStatusCode();
    if (err >= 0) {
      #ifdef DEBUG
        Serial.print("Got status code: ");
        Serial.println(err);
      #endif
      // Usually you'd check that the response code is 200 or a
      // similar "success" code (200-299) before carrying on,
      // but we'll print out whatever response we get

      err = http.skipResponseHeaders();
      if (err >= 0)
      {
        int bodyLen = http.contentLength();
        #ifdef DEBUG
          Serial.print("Content length is: ");
          Serial.println(bodyLen);
          Serial.println();
          Serial.println("Body returned follows:");
        #endif
        // Now we've got to the body, so we can print it out
        unsigned long timeoutStart = millis();
        char c;
        // Whilst we haven't timed out & haven't reached the end of the body
        while ( (http.connected() || http.available()) &&
               ((millis() - timeoutStart) < kNetworkTimeout) )
        {
            if (http.available())
            {
                c = http.read();
                // Print out this character
                #ifdef DEBUG
                Serial.print(c);
                #endif
                bodyLen--;
                // We read something, reset the timeout counter
                timeoutStart = millis();
            }
            else
            {
                // We haven't got any data, so let's pause to allow some to
                // arrive
                delay(kNetworkDelay);
            }
        }
      }
      else
      {
        #ifdef DEBUG
        Serial.print("Failed to skip response headers: ");
        Serial.println(err);
        #endif
      }
    }
    else
    {    
      #ifdef DEBUG
      Serial.print("Getting response failed: ");
      Serial.println(err);
      #endif
    }
  }
  else
  {
    #ifdef DEBUG
    Serial.print("Connect failed: ");
    Serial.println(err);
    #endif
  }
  http.stop();
}


