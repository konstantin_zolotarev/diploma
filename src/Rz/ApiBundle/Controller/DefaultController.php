<?php

namespace Rz\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\Response;
use \Rz\MainBundle\Entity\Transformator;
use \Rz\MainBundle\Entity\TransParams;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return new Response(1);
    }

    /**
     *
     * @Route("/stat/{id}/{temp}/{diff}/{u}/{i}")
     * @ParamConverter("trans", class="RzMainBundle:Transformator")
     */
    public function statAction(Transformator $trans, $temp, $diff, $u, $i) {
      $param = new TransParams();
      $param->setTransformator($trans);
      $param->setTemperature((float)$temp);
      $param->setDiffProtection((float)$diff);
      $param->setVoltage((float)$u);
      $param->setAmperage((float)$i);
      $trans->addTransParams($param);
      $this->getDoctrine()->getEntityManager()->flush();
      return new Response(1);
    }

    /**
     * Power off or on
     * @Route("/power/{id}/{state}")
     * @ParamConverter("trans", class="RzMainBundle:Transformator")
     */
    public function powerAction(Transformator $trans, $state) {
      if ($state == 1) {
        $status = Transformator::STATUS_WORKING;
      } else {
        $status = Transformator::STATUS_OFF;
      }
      $trans->setStatus($status);
      $param = new TransParams();
      $param->setTransformator($trans);
      $param->setState($status);
      $trans->addTransParams($param);
      $this->getDoctrine()->getEntityManager()->flush();
      return new Response(1);
    }

    /**
     *
     * @Route("/ping")
     */
    public function pingAction() {
        return new Response(1);
    }

    
}
