<?php

namespace Rz\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Rz\MainBundle\Entity\Transformator;
use \Rz\MainBundle\Entity\TransParams;
use \Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation\Inject;

class DefaultController extends Controller
{

  /**
   *
   * @Inject("arduino")
   * @var Rz\MainBundle\Service\Arduino;
   */
  public $arduino;

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
      $list = $this->getDoctrine()->getRepository("RzMainBundle:Transformator")
              ->findAll();
        return array(
            'list'  => $list
        );
    }

    /**
     *
     * @Route("/{id}.html", name="trans_stat")
     * @ParamConverter("trans", class="RzMainBundle:Transformator")
     * @Template()
     * @param \Rz\MainBundle\Entity\Transformator $trans
     */
    public function statAction(Transformator $trans) {

      return array(
          'trans' => $trans
      );
    }

    /**
     *
     * @Route("/period/{id}.html")
     * @ParamConverter("trans", class="RzMainBundle:Transformator")
     */
    public function periodAction(Transformator $trans) {
      $dateStart = $this->getRequest()->get("start", false);
      $dateStop  = $this->getRequest()->get("stop", false);
      /* @var $em \Doctrine\ORM\EntityManager */
      $em = $this->getDoctrine()->getEntityManager();
      $q = "SELECT tp FROM Rz\MainBundle\Entity\TransParams tp WHERE tp.transformator = :id";
      if ($dateStart) {
        $q .= " AND tp.created >= '".$dateStart."'";
      }
      if ($dateStop && strtotime($dateStart) < strtotime($dateStop)) {
        $q .= " AND tp.created < '".$dateStop."'";
      }
      $dql = $em->createQuery($q);
      $dql->setParameter("id", $trans->getId());
      $list = $dql->getArrayResult();
      return new Response(json_encode($list));
    }

    /**
     * @Route("/stat/{id}.html")
     * @ParamConverter("trans", class="RzMainBundle:Transformator")
     * @Template()
     * @param \Rz\MainBundle\Entity\Transformator $trans
     */
    public function currentAction(Transformator $trans) {
//      $param = new TransParams();
//      $param->setDiffProtection(rand(0, 5)+rand(1, 99)/100);
//      $param->setTemperature(rand(50, 60));
//      $param->setTransformator($trans);
//      $trans->addTransParams($param);
//      $this->getDoctrine()->getEntityManager()->flush();
      $param = $trans->getLastParam();
      if (!$param) {
        $param = new TransParams();
        $param->setDiffProtection(0);
        $param->setTemperature(0);
        $param->setTransformator($trans);
        $trans->addTransParams($param);
      }
      return new Response(json_encode($param));
    }

    /**
     *
     * @Route("/check/{id}")
     * @ParamConverter("trans", class="RzMainBundle:Transformator")
     * @Template()
     */
    public function checkAction(Transformator $trans) {
      $status = $this->arduino->check();
      if ($status === false) {
        //offline
        $trans->setStatus(Transformator::STATUS_ERROR);
        $this->getDoctrine()->getEntityManager()->flush();
      } else {
        if ($status == 1) {
            $this->turnTrans($trans, 1);
        } else {
            $this->turnTrans($trans, 0);
        }
      }
      return array(
          'trans' => $trans
      );
    }

    /**
     *
     * @Route("/turn/{id}/{on}")
     * @ParamConverter("trans", class="RzMainBundle:Transformator")
     * @Template()
     * @param \Rz\MainBundle\Entity\Transformator $trans
     */
    public function turnAction(Transformator $trans, $on) {
      if ($on == 1) {
        $this->arduino->turnOn();
        $this->turnTrans($trans, Transformator::STATUS_WORKING);
      } else {
        $this->arduino->turnOff();
        $this->turnTrans($trans, Transformator::STATUS_OFF);
      }
      return array(
          'trans' => $trans
      );
    }

    /**
     *
     * @param \Rz\MainBundle\Entity\Transformator $trans
     * @param int $status
     */
    protected function turnTrans(Transformator $trans, $status) {
      $trans->setStatus($status);
      $param = new TransParams();
      $param->setDiffProtection(0);
      $param->setTemperature(0);
      $param->setTransformator($trans);
      $param->setState($status);
      $trans->addTransParams($param);
      $this->getDoctrine()->getEntityManager()->flush();
    }
}
