<?php

namespace Rz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="equipment_params")
 */
class TransParams {

  /**
   *
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  public $id;

  /**
   *
   * @ORM\ManyToOne(targetEntity="Transformator")
   */
  protected $transformator;

  /**
   *
   * @ORM\Column(type="float")
   */
  public $diff_protection = 0;

  /**
   *
   * @ORM\Column(type="float")
   */
  public $temperature = 0;

  /**
   *
   * @ORM\Column(type="float")
   */
  public $voltage = 0;

  /**
   *
   * @ORM\Column(type="float")
   */
  public $amperage = 0;

  /**
   *
   * @ORM\Column(type="smallint")
   */
  public $state = 1;

  /**
   *
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(type="datetime")
   */
  protected $created;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set temperature
   *
   * @param float $temperature
   */
  public function setTemperature($temperature) {
    $this->temperature = $temperature;
  }

  /**
   * Get temperature
   *
   * @return float
   */
  public function getTemperature() {
    return $this->temperature;
  }

  /**
   * Set created
   *
   * @param datetime $created
   */
  public function setCreated($created) {
    $this->created = $created;
  }

  /**
   * Get created
   *
   * @return datetime
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   *
   * @return string datetime
   */
  public function getCreatedAt() {
    return $this->created->format("Y-m-d H:i:s");
  }

  /**
   * Set transformator
   *
   * @param Rz\MainBundle\Entity\Transformator $transformator
   */
  public function setTransformator(\Rz\MainBundle\Entity\Transformator $transformator) {
    $this->transformator = $transformator;
  }

  /**
   * Get transformator
   *
   * @return Rz\MainBundle\Entity\Transformator
   */
  public function getTransformator() {
    return $this->transformator;
  }

  /**
   * Set diff_protection
   *
   * @param float $diffProtection
   */
  public function setDiffProtection($diffProtection) {
    $this->diff_protection = $diffProtection;
  }

  /**
   * Get diff_protection
   *
   * @return float
   */
  public function getDiffProtection() {
    return $this->diff_protection;
  }

  /**
   * Set state
   *
   * @param smallint $state
   */
  public function setState($state) {
    $this->state = $state;
  }

  /**
   * Get state
   *
   * @return smallint
   */
  public function getState() {
    return $this->state;
  }

  /**
   * Set voltage
   *
   * @param float $voltage
   */
  public function setVoltage($voltage) {
    $this->voltage = $voltage;
  }

  /**
   * Get voltage
   *
   * @return float
   */
  public function getVoltage() {
    return $this->voltage;
  }

  /**
   * Set amperage
   *
   * @param float $amperage
   */
  public function setAmperage($amperage) {
    $this->amperage = $amperage;
  }

  /**
   * Get amperage
   *
   * @return float
   */
  public function getAmperage() {
    return $this->amperage;
  }

}