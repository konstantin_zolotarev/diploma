<?php

namespace Rz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="equipment")
 */
class Transformator {

  const STATUS_OFF = 0;
  const STATUS_WORKING = 1;
  const STATUS_ERROR = 2;

  /**
   *
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  public $id;

  /**
   *
   * @ORM\Column(type="string")
   */
  public $title;

  /**
   *
   * @ORM\Column(type="string")
   */
  public $serial;

  /**
   *
   * @ORM\OneToMany(targetEntity="TransParams", mappedBy="transformator", cascade={"persist", "remove"})
   */
  public $params;

  /**
   *
   * @ORM\Column(type="smallint")
   */
  public $status = 0;

  /**
   *
   * @return TransParams|boolean last params
   */
  public function getLastParam() {
    if ($this->getParams()->count() > 0) {
      return $this->getParams()->last();
    }
    return false;
  }

  public function __construct() {
    $this->params = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set title
   *
   * @param string $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Get title
   *
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Add params
   *
   * @param Rz\MainBundle\Entity\TransParams $params
   */
  public function addTransParams(\Rz\MainBundle\Entity\TransParams $params) {
    $this->params[] = $params;
  }

  /**
   * Get params
   *
   * @return Doctrine\Common\Collections\Collection
   */
  public function getParams() {
    return $this->params;
  }


    /**
     * Set status
     *
     * @param smallint $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return smallint 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set serial
     *
     * @param string $serial
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;
    }

    /**
     * Get serial
     *
     * @return string 
     */
    public function getSerial()
    {
        return $this->serial;
    }
}