<?php

namespace Rz\MainBundle\Service;

use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Inject;

/**
 * Description of Arduino
 *
 * @Service("arduino")
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 */
class Arduino {

  const ARDUINO_URL = "http://192.168.180.229/";

  public function turnOff() {
    return $this->call("off");
  }

  public function turnOn() {
    return $this->call("on");
  }

  /**
   * Check current state
   * @return type
   */
  public function check() {
    return $this->call("");
  }

  protected function call($path) {
    $ch = curl_init();

    // установка URL и других необходимых параметров
    curl_setopt($ch, CURLOPT_URL, self::ARDUINO_URL.$path);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); //set 5 sec to connect to arduino

    // загрузка страницы и выдача её браузеру
    $result = curl_exec($ch);

    // завершение сеанса и освобождение ресурсов
    curl_close($ch);
    return $result;
  }
}