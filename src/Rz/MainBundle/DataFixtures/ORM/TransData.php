<?php

namespace Rz\MainBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Rz\MainBundle\Entity\Transformator;
use \Rz\MainBundle\Entity\TransParams;

/**
 * Description of TransData
 * 
 * @package Expression Package is undefined on line 10, column 15 in Templates/Scripting/PHPClass.php.
 * @subpackage Expression SubPackage is undefined on line 11, column 18 in Templates/Scripting/PHPClass.php.
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 */
class TransData implements FixtureInterface {

  public function load(ObjectManager $manager) {
    
    $trans = new Transformator();
    $trans->setTitle("25 МВА");
    $trans->setSerial("ASMD13248132-12341");
    $manager->persist($trans);

//    for($i = 0; $i < 100; $i++) {
//      $transParam = new TransParams();
//      $transParam->setState(Transformator::STATUS_WORKING);
//      $transParam->setTemperature(rand(50, 65));
//      $transParam->setDiffProtection((rand(0, 3)+rand(0, 100)/100));
//      $transParam->setCreated(new \DateTime(date("Y-m-d H:i:s", strtotime("+".$i." minutes"))));
//      $transParam->setTransformator($trans);
//      $trans->addTransParams($transParam);
//    }

    $trans = new Transformator();
    $trans->setTitle("110/10 25 МВА");
    $trans->setSerial("NHFYC-1212312-123");
    $manager->persist($trans);
    
    $manager->flush();
  }
}