<?php

namespace Rz\UserBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Rz\UserBundle\Entity\User;

/**
 * Description of UserData
 * 
 * @package Expression Package is undefined on line 10, column 15 in Templates/Scripting/PHPClass.php.
 * @subpackage Expression SubPackage is undefined on line 11, column 18 in Templates/Scripting/PHPClass.php.
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 */
class UserData implements FixtureInterface {

  public function load(ObjectManager $manager) {
    $userAdmin = new User();
    $userAdmin->setUsername('admin');
    $userAdmin->setPlainPassword('admin');
    $userAdmin->setEmail("konstantin.zolotarev@gmail.com");
    $userAdmin->setEnabled(true);
    $userAdmin->addRole("ROLE_SUPER_ADMIN");
    $userAdmin->addRole("ROLE_ADMIN");

    $manager->persist($userAdmin);

    $userAdmin = new User();
    $userAdmin->setUsername('user');
    $userAdmin->setPlainPassword('user');
    $userAdmin->setEmail("konstantin.zolotarev.work@gmail.com");
    $userAdmin->setEnabled(true);
    $userAdmin->addRole("ROLE_USER");

    $manager->persist($userAdmin);
    $manager->flush();
  }

}