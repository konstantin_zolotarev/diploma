<?php

namespace Rz\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
//use JMS\DiExtraBundle\Annotation\Inject;

/**
 * Description of User
 * 
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @author Konstantin Zolotarev <konstantin.zolotarev@gmail.com>
 */
class User extends BaseUser {

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId() {
    return $this->id;
  }

  public function isAuthenticated() {
    return $this->hasRole("IS_AUTHENTICATED_FULLY");
  }
}